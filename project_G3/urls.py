from django.contrib import admin
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static

from . import views
from comm import views as community_views

urlpatterns = [
	path('', views.index, name='index'),
	path('us', views.aboutUs, name='aboutUs'),
	path('postCreate', views.postCreate, name='postCreate'),
	path('<int:post_id>/', views.detail, name='detail'),
	path("communityCreate", community_views.communityCreate, name = 'communityCreate'),
	path('api/post/<int:post_id>/like', views.api_like),
    path('api/post/<int:post_id>/dislike', views.api_dislike),
	path('<int:post_id>/delete', views.delete, name='delete'),
	path('comments/<int:comment_id>/delete', views.delete_comment, name='delete_comment'),
	path('comments/<int:comment_id>/reply', views.reply_comment, name='reply_comment'),
	path('comments/reply/<int:reply_id>/delete', views.delete_reply, name='delete_reply'),
	path('<int:post_id>/edit', views.editPost, name='editPost'),
	path('editprofile', views.editProfile, name='editProfile'),
	path('<int:post_id>/savePost', views.savePost, name='savePost'),
	path('mainpage', views.mainpage, name = 'mainpage'),
	path('api/post/<int:post_id>/save', views.APIsave, name= 'APIsave'),

]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
