from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
import datetime

from django.core.cache import cache 
from config import settings
from datetime import timedelta


# Create your models here.

class Community(models.Model):
    name = models.TextField()
    description = models.TextField()    
    created_at = models.DateTimeField(default = timezone.now)
    admin = User()
    members = models.ManyToManyField(User, default=None, blank= True)
    background = models.ImageField(upload_to='upload/communityBackground/', null=True, blank=True, default = "upload/communityBackground/default-background.jpg")
    avatar = models.ImageField(upload_to='upload/communityAvatar/', null=True, blank=True, default = "upload/communityAvatar/default-avatar.png")
    def __str__(self):
        return self.name
    def getOnlineUsers(self):
        # print(self)
        numOnlineUsers = 0
        for user in self.members.all():
            userProfile = UserProfile.objects.get(user = user)
            # print(user)
            # print(timezone.now()-  timedelta( hours=12))
            if (user.last_login > userProfile.lastLogout and timezone.now()-timedelta( hours=12)<user.last_login ):
                
                numOnlineUsers+=1
        return numOnlineUsers

        
class Post(models.Model):
    title= models.CharField(max_length=200)
    community = models.ForeignKey(Community, on_delete=models.CASCADE, null=True, blank= True)
    body = models.TextField(blank= True, null= True) 
    posted_at = models.DateTimeField(default= timezone.now)
    image = models.FileField(upload_to='upload/images/', null=True, blank=True,)
    video = models.FileField(upload_to="upload/videos/",null=True, blank=True,)
    audio = models.FileField(upload_to="upload/audios/",null=True, blank=True,)
    published_at = models.DateTimeField(blank= True, null= True)
    # like= models.IntegerField(default=0)
    # dislike= models.IntegerField(default=0)
    author = models.ForeignKey(User, on_delete=models.CASCADE, null=True, related_name='author')
    liked= models.ManyToManyField(User, default=None, blank= True, related_name='liked')
    disliked= models.ManyToManyField(User, default=None, blank= True, related_name='disliked')



    def publish(self):  
        self.published_at= timezone.now()
        self.save
    
    def __str__(self):
        return self.title

    @property
    def num_likes(self):
        return self.liked.all().count()

    @property
    def num_dislikes(self):
        return self.disliked.all().count()

class UserProfile(models.Model):
    user   = models.OneToOneField(User, on_delete=models.CASCADE)
    displayname= models.TextField(blank= True, null= True)
    avatar = models.ImageField(upload_to='upload/userAvatar/', null=True, blank=True,default='upload/userAvatar/default.png')
    createdposts = models.ManyToManyField(Post, default=None, blank= True, related_name='createdposts')
    birth = models.DateField(blank= True, null= True)
    about = models.TextField(blank= True, null= True)
    countLikes= models.IntegerField(default=0)
    countDislikes= models.IntegerField(default=0)
    lastLogout = models.DateTimeField(default= datetime.datetime(2000, 2, 15, 23, 59, 59, 415321))
    saved_posts = models.ManyToManyField(Post, default=None, blank= True, related_name='saved_posts')
    followed_users= models.ManyToManyField(User, default=None, blank= True, related_name='followed_users')

    def __str__(self):
        return self.user.username

    # def last_seen(self):
    #     print(cache.get('seen_%s' % self.user.username))
    #     return cache.get('seen_%s' % self.user.username)

    # def online(self):
    #     if self.last_seen():
    #         now = datetime.datetime.now()
    #         if now > self.last_seen() + datetime.timedelta(
    #                 seconds=settings.USER_ONLINE_TIMEOUT):
    #             return False
    #         else:
    #             return True
    #     else:
    #         return False

    def isOnline(self):
        # print(settings.USER_ONLINE_TIMEOUT)
        if (self.user.last_login > self.lastLogout and timezone.now()-timedelta( hours=settings.USER_ONLINE_TIMEOUT)<self.user.last_login ):
            return True
        if (self.user.last_login > self.lastLogout and timezone.now()-timedelta( hours=settings.USER_ONLINE_TIMEOUT)>self.user.last_login):
            self.lastLogout= self.user.last_login+timedelta( hours=settings.USER_ONLINE_TIMEOUT)
            self.save()
        return False

    @property
    def pixelLikes(self):
        return 123*(self.countLikes)/(self.countDislikes+self.countLikes)
    
    @property
    def pixelDislikes(self):
        return 123-123*(self.countLikes)/(self.countDislikes+self.countLikes)

# LIKE_CHOICES= (
#     ('Like', 'Like'),
#     ('Unlike', 'Unlike'),
# )

# class Like(models.Model):
#     user= models.ForeignKey(User, on_delete=models.CASCADE)
#     post= models.ForeignKey(Post, on_delete=models.CASCADE)
#     value= models.CharField(choices=LIKE_CHOICES, default='Like', max_length=10)
    
#     def __str__(self):
#         return str(self.post)

class Comment(models.Model):
    text= models.TextField()
    posted_at = models.DateTimeField(default= timezone.now)
    article = models.ForeignKey( Post, related_name= "comments", on_delete= models.CASCADE)
    posted_by = models.ForeignKey(User, on_delete=models.CASCADE, null=True, related_name='cposted_by')

    def __str__(self):
        return str(self.posted_by) + ': '+ str(self.text) 

class Reply(models.Model):
    text= models.TextField()
    posted_at = models.DateTimeField(default= timezone.now)
    comment = models.ForeignKey(Comment, related_name= "replies", on_delete= models.CASCADE)
    posted_by = models.ForeignKey(User, on_delete=models.CASCADE, null=True, related_name='rposted_by')

# class Video(models.Model):
#     caption = models.CharField(max_length=100)
#     video = models.FileField(upload_to="video/%y")
#     def __str__(self):
#         return self.caption


    