from django.shortcuts import render, redirect
from django.http import HttpResponse, Http404, JsonResponse
from django.utils import timezone
from django.views.generic import TemplateView, ListView, CreateView
from django.core.files.storage import FileSystemStorage
from django.urls import reverse_lazy
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.db.models import Count
from project_G3.models import Post, Comment, Community, UserProfile, Reply
import random

app_name = 'project_G3'


def getRandomAds():
    imgList = ["https://i.pinimg.com/originals/53/c6/d5/53c6d53c0a420d130ea23d645533c934.gif",
               "https://i.pinimg.com/originals/b0/1d/df/b01ddf257d18c8a9b41e0502161d580c.gif",
               "https://miro.medium.com/max/1400/1*VuRMGPKL5vgl_22OhhnbiQ.gif",
               "https://miro.medium.com/max/1400/1*Jy91LipOh3Pfl9GZugQLBw.gif",
               "https://media.giphy.com/media/3o6ZsVllI9P8PpM0Ao/giphy.gif",
               ]
    return imgList[random.randint(0, len(imgList) - 1)]


def getUserCommunity(request):
    try:
        communities = Community.objects.filter(members=request.user)
    except:
        communities = Community.objects.none()
    return communities
# Create your views here.


def sort(request, posts):
    if 'sort' in request.GET:
        if request.GET['sort'] == 'date':
            posts = posts.order_by('posted_at')
        elif request.GET['sort'] == 'like':
            posts = posts.annotate(like_count=Count(
                'liked')).order_by('-like_count')
        elif request.GET['sort'] == 'dislike':
            posts = posts.annotate(dislike_count=Count(
                'disliked')).order_by('-dislike_count')
        return posts
    else:
        return posts.order_by('-posted_at')


def index(request):


    communities = getUserCommunity(request)
    try:
        posts = Post.objects.filter(community__in=communities) | Post.objects.filter(
            author=request.user) | Post.objects.filter(author__in=request.user.userprofile.followed_users.all())
    except:
        posts = Post.objects.none()
    suggestedPost = []
    for post in Post.objects.all().annotate(like_count=Count('liked')).order_by('-like_count'):
        if (post not in posts):
            suggestedPost += [post]
    # print(suggestedPost)
    posts = sort(request, posts)
    if request.user.is_authenticated:
        player, created = UserProfile.objects.get_or_create(user=request.user)
    topCommunities = Community.objects.annotate(
        rank=Count('members')).order_by('-rank')[:5]
    newCommunities = Community.objects.all().order_by('-created_at')[:5]
    data = {
        'communities': communities,
        'posts': posts,
        'topCommunities': topCommunities,
        'newCommunities': newCommunities,
        'action': 'Home',
        'actionURL': "https://i.imgur.com/zmZkm8I.png",
        'ads': getRandomAds(),
        'suggestedPosts': suggestedPost[:5],
        'description': "Your personal FReddit frontpage. Come here to check in with your favorite communities.",
    }
    if request.user.is_authenticated:
        data['user'] = request.user
    else:
        data['user'] = None
    return render(request, 'project_G3/index.html', data)


def mainpage(request):
    communities = getUserCommunity(request)
    posts = sort(request, Post.objects)
    if request.user.is_authenticated:
        player, created = UserProfile.objects.get_or_create(user=request.user)
    topCommunities = Community.objects.annotate(
        rank=Count('members')).order_by('-rank')[:5]
    newCommunities = Community.objects.all().order_by('-created_at')[:5]
    data = {
        'communities': communities,
        'posts': posts,
        'topCommunities': topCommunities,
        'newCommunities': newCommunities,
        'action': 'Mainpage',
        'actionURL': "https://i.imgur.com/uOQZrhr.png",
        'description': "The most active posts from all of FReddit. Come here to see new posts rising and be a part of the conversation.",
        'ads': getRandomAds()
    }
    if request.user.is_authenticated:
        data['user'] = request.user
    else:
        data['user'] = None

    return render(request, 'project_G3/index.html', data)


def login(request):
    return render(request, 'auth/login.html')


@login_required(login_url='/auth/login/')
def postCreate(request):
    communities = getUserCommunity(request)
    community = ""
    content = {
        'communities': communities,
        'action': "Create post",
        'actionURL': "https://i.imgur.com/k3VFgYR.png",
    }
    if 'community' in request.GET:
        community = Community.objects.get(name=request.GET['community'])
        content["community"] = community
    if request.method == 'POST':
        # print(community.name)
        post = Post(title=request.POST['title'],
                    body=request.POST['text'],
                    posted_at=timezone.now(),
                    author=request.user,
                    )
        try:
            post.community = community
        except:
            print("There is no community")
        if not post.title:
            messages.info(request, 'Title is required')
            return render(request, 'project_G3/postCreate.html', content)
        try:
            
            print(str(request.FILES['media'])[-3:])
            if str(request.FILES['media'])[-3:]=='mp4' or str(request.FILES['media'])[-3:]=='m4v':
                post.video = request.FILES['media']
            elif str(request.FILES['media'])[-3:]=='mp3':
                post.audio = request.FILES['media']
            else:
                post.image = request.FILES['media']
        except:
            print("No media files")
        post.save()
        request.user.userprofile.createdposts.add(post)
        return redirect(detail, post_id=post.id)
    return render(request, 'project_G3/postCreate.html', content)


def detail(request, post_id):
    communities = getUserCommunity(request)
    try:
        post = Post.objects.get(pk=post_id)
    except Post.DoesNotExist:
        raise Http404("Post does not exist")

    try:
        if post in request.user.userprofile.saved_posts.all():
            if request.method == "POST":
                if request.user.is_authenticated:
                    comment = Comment(
                        article=post, text=request.POST['text'], posted_by=request.user)
                    comment.save()
                    return redirect('/'+str(post_id)+"#cm")
                else:
                    messages.info(
                        request, 'You must be logged in to post a comment')
                    return render(request, "project_G3/detail.html", context)
            context = {
                "saved": "saved",
                "post": post,
                "comments": post.comments.all(),
                "comments_count": post.comments.all().count(),
                "viewing_user": request.user.username,
                'communities': communities,
                "action": "Post",
                "actionURL": "https://i.imgur.com/k3VFgYR.png",
                "ads" : getRandomAds()
            }
            return render(request, "project_G3/detail.html", context)

    except:
        context = {
            "post": post,
            "comments": post.comments.all(),
            "user": None,
            "comments_count": post.comments.all().count(),
            "viewing_user": request.user.username,
            'communities': communities,
            "action": "Post",
            "actionURL": "https://i.imgur.com/k3VFgYR.png",
            "ads" : getRandomAds()
        }
    if request.method == "POST":
        if request.user.is_authenticated:
            comment = Comment(
                article=post, text=request.POST['text'], posted_by=request.user)
            comment.save()
            return redirect('/'+str(post_id)+"#cm")
        else:
            messages.info(request, 'You must be logged in to post a comment')
            return render(request, "project_G3/detail.html", context)
    context = {
        "post": post,
        "comments": post.comments.all(),
        "comments_count": post.comments.all().count(),
        "viewing_user": request.user.username,
        'communities': communities,
        "action": "Post",
        "actionURL": "https://i.imgur.com/k3VFgYR.png",
        "ads" : getRandomAds()
    }
    return render(request, "project_G3/detail.html", context)


@login_required(login_url='/auth/login/')
def delete(request, post_id):
    try:
        post = Post.objects.get(pk=post_id)
    except Post.DoesNotExist:
        raise Http404("Post has already been deleted or doesn't exist")
    if request.user == post.author:
        post.author.userprofile.countLikes -= post.num_likes
        post.author.userprofile.countDislikes -= post.num_dislikes
        post.author.userprofile.save()
        post.delete()
    return redirect(index)


@login_required(login_url='/auth/login/')
def delete_comment(request, comment_id):
    try:
        comment = Comment.objects.get(pk=comment_id)
        post_id2 = comment.article_id
        # print(post_id2)
    except Comment.DoesNotExist:
        raise Http404("Comment has already been deleted or doesn't exist")
    comment.delete()
    return redirect('/'+str(post_id2)+"#cm")


def reply_comment(request, comment_id):
    try:
        comment = Comment.objects.get(pk=comment_id)
        post_id = comment.article_id
    except Comment.DoesNotExist:
        raise Http404("Comment does not exist")
    if request.method == "POST":
        if request.user.is_authenticated:
            reply = Reply(
                comment=comment, text=request.POST['reply_comment'], posted_by=request.user)
            reply.save()
            return redirect('/'+str(post_id)+'#cm')
        else:
            messages.info(request, 'You must be logged in to post a reply')
            return redirect('/'+str(post_id)+'#cm')


@login_required(login_url='/auth/login/')
def delete_reply(request, reply_id):
    try:
        reply = Reply.objects.get(pk=reply_id)
        post_id = reply.comment.article_id
    except Reply.DoesNotExist:
        raise Http404("Reply has already been deleted or doesn't exist")
    reply.delete()
    return redirect('/'+str(post_id)+'#cm')


@login_required(login_url='/auth/login/')
def editPost(request, post_id):
    try:
        post = Post.objects.get(pk=post_id)
    except Post.DoesNotExist:
        raise Http404("Post has already been deleted or doesn't exist")
    if request.method == "POST":
        post.body = request.POST.get('edit_post')
        post.save()
        return redirect(detail, post_id=(post.id))


@login_required(login_url='/auth/login/')
def savePost(request, post_id):
    try:
        post = Post.objects.get(pk=post_id)
    except Post.DoesNotExist:
        raise Http404("Post has already been deleted or doesn't exist")
    # Check if a post has already been saved in the database or not
    if not post in request.user.userprofile.saved_posts.all():
        request.user.userprofile.saved_posts.add(post)
        previous_page = request.META['HTTP_REFERER']
        return redirect(previous_page)
    else:
        request.user.userprofile.saved_posts.remove(post)
        previous_page = request.META['HTTP_REFERER']
        return redirect(previous_page)


def APIsave(request, post_id):
    try:
        post = Post.objects.get(pk=post_id)
    except Post.DoesNotExist:
        raise Http404("Post has already been deleted or doesn't exist")
    action = ''

    if not post in request.user.userprofile.saved_posts.all():
        request.user.userprofile.saved_posts.add(post)
        action = 'Unsave'
    else:
        request.user.userprofile.saved_posts.remove(post)
        action = 'Save'

    result = {
        'post_id': post.id,
        'action': action
    }
    return JsonResponse(result)


def api_like(request, post_id):
    if not request.user.is_authenticated:
        result = {
            'loginrequired': 1,
        }
        return JsonResponse(result)
    try:
        # post = Post.objects.get(pk=post_id)
        # post.like += 1
        # post.save()
        post = Post.objects.get(id=post_id)
        user = request.user
        action = ''
        conflict = 0
        if user in post.liked.all():
            post.liked.remove(user)
            action = 'unlike'
            post.author.userprofile.countLikes -= 1
            post.author.userprofile.save()
        else:
            post.liked.add(user)
            action = 'like'
            if user in post.disliked.all():
                conflict = 1
            post.author.userprofile.countLikes += 1
            post.author.userprofile.save()

    except Post.DoesNotExist:
        # print('hi')
        raise Http404("Post does not exist")

    result = {
        'id': post_id,
        'like': post.num_likes,
        'action': action,
        'conflict': conflict,
    }

    return JsonResponse(result)


def api_dislike(request, post_id):
    if not request.user.is_authenticated:
        result = {
            'loginrequired': 1,
        }
        return JsonResponse(result)
    try:
        post = Post.objects.get(id=post_id)
        user = request.user
        action = ''
        conflict = 0
        if user in post.disliked.all():
            post.disliked.remove(user)
            action = 'undislike'
            post.author.userprofile.countDislikes -= 1
            post.author.userprofile.save()
        else:
            post.disliked.add(user)
            action = 'dislike'
            if user in post.liked.all():
                conflict = 1
            post.author.userprofile.countDislikes += 1
            post.author.userprofile.save()
    except Post.DoesNotExist:
        raise Http404("Post does not exist")

    result = {
        'id': post_id,
        'dislike': post.num_dislikes,
        'action': action,
        'conflict': conflict,
    }

    return JsonResponse(result)


@login_required(login_url='/auth/login/')
def editProfile(request):
    if request.method == "POST":
        profile = request.user.userprofile
        if request.POST['date']:
            profile.birth = request.POST['date']

        profile.displayname = request.POST['displayname']

        profile.about = request.POST['about']
        try:
            profile.avatar = request.FILES['newAvatar']
        except:
            print("No image uploaded")
        profile.save()
        messages.info(request, 'Your profile has been updated!')
    communities = getUserCommunity(request)
    content = {
        'communities': communities,
        "action": "Edit your profile",
        "actionURL": "https://i.imgur.com/nWmEA9L.png",
    }
    return render(request, 'project_G3/editUserProfile.html', content)


def aboutUs(request):
    return render(request, 'project_G3/aboutUs.html')
