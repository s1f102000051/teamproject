# Generated by Django 3.1.3 on 2021-01-26 00:13

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('project_G3', '0057_video'),
    ]

    operations = [
        migrations.AlterField(
            model_name='post',
            name='image',
            field=models.FileField(blank=True, null=True, upload_to='upload/images/'),
        ),
    ]
