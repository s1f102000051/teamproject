# Generated by Django 3.1.3 on 2020-12-02 07:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('project_G3', '0016_auto_20201202_1546'),
    ]

    operations = [
        migrations.AlterField(
            model_name='post',
            name='posted_by',
            field=models.CharField(blank=True, max_length=20, null=True),
        ),
    ]
