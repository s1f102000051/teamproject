# Generated by Django 3.1.3 on 2020-12-02 16:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('project_G3', '0018_auto_20201202_2201'),
    ]

    operations = [
        migrations.AddField(
            model_name='comment',
            name='posted_by',
            field=models.CharField(blank=True, max_length=20, null=True),
        ),
    ]
