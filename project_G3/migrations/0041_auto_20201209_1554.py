# Generated by Django 3.1.3 on 2020-12-09 06:54

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('project_G3', '0040_auto_20201208_2249'),
    ]

    operations = [
        migrations.AlterField(
            model_name='community',
            name='created_at',
            field=models.DateTimeField(default=datetime.datetime(2020, 12, 9, 6, 54, 39, 439399, tzinfo=utc)),
        ),
    ]
