import datetime
from django.core.cache import cache
from django.conf import settings
from django.utils.deprecation import MiddlewareMixin

class ActiveUserMiddleware(MiddlewareMixin):

    def process_request(self, request):
        current_user = request.user
        # print(settings.USER_LASTSEEN_TIMEOUT)
        if request.user.is_authenticated:
            now = datetime.datetime.now()
            # print(current_user)
            cache.set('seen_%s' % (current_user.username), now,  settings.USER_LASTSEEN_TIMEOUT)
            # print(cache.get('seen_%s' % (current_user.username)))
            # print('-')
