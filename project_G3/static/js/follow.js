function callback_follow(json){
    console.log(json);
    if (json.loginrequired)
        alert("You must be logged in to follow someone");
    
    var element = document.getElementById("follow"+json.followuser);
    element.textContent = json.state;
    element = document.getElementById("unfollow" + json.followuser);
    if (json.state == "followed"){
        element.textContent = "unfollow";
    }
    else element.textContent = "follow";
    // change background color
    element = document.getElementById("followButton"+json.followuser);
    if (json.state == "followed"){
        element.style = "background-color:#ffffff;color: #24A0ED;";
    }
    else element.style = "background-color: #24A0ED; color: #ffffff;";
}

function followButton(followinguser){
    var req = new XMLHttpRequest();
    // console.log(1);
    // console.log(username);
    req.onreadystatechange = function () {
        if ((req.readyState == 4) && (req.status == 200)) {
            // console.log(2);
            var json = JSON.parse(req.responseText);
            callback_follow(json);
        }
    }
    // console.log(3);

    req.open('GET', '/u/' + followinguser + '/follow');
    req.send(null);
}

