function callback_join(json){
    if (json.loginrequired)
        alert("You must be logged in to join a community")
    var element = document.getElementById("join"+json.community_name);
    element.textContent = json.state;
    element = document.getElementById("leave" + json.community_name);
    if (json.state == "joined"){
        element.textContent = "leave";
    }
    else element.textContent = "join";
    // change background color
    element = document.getElementById("joinButton"+json.community_name);
    if (json.state == "joined"){
        element.style = "background-color:#ffffff;color: #24A0ED;";
    }
    else element.style = "background-color: #24A0ED; color: #ffffff;";

    element = document.getElementById("members");
    element.textContent = json.members; 
    element = document.getElementById("onlineUsers");
    element.textContent = json.onlineUsers;
}
function joinButton(community_name){
    var req = new XMLHttpRequest();
    // console.log(1);
    req.onreadystatechange = function () {
        if ((req.readyState == 4) && (req.status == 200)) {
            // console.log(2);
            var json = JSON.parse(req.responseText);
            callback_join(json);
        }
    }
    // console.log(3);

    req.open('GET', '/r/' + community_name + '/join');
    req.send(null);
}