from django.shortcuts import render, redirect
from django.http import HttpResponse, Http404, JsonResponse
from django.utils import timezone
from django.views.generic import TemplateView, ListView, CreateView
from django.contrib import messages
from django.db.models import Q,F
from project_G3.models import Post, Comment, Community
from django.contrib.auth.models import User
from django.db.models import Count
from django.contrib.postgres.search import SearchQuery, SearchRank, SearchVector
from project_G3.views import sort, getUserCommunity
# Create your views here.
def searchContent(request):
    if request.method == 'GET':
        if ('q' in request.GET):
            search_term = request.GET.get('q').split(" ")
            # print(search_term)
            # search by posts
            #search by users/community
            posts = []
            searchCommunities = []
            searchUsers =[]
            users = []
            for q in search_term:
                # print(q)
                # post
                query = Post.objects.filter(Q(title__icontains = q) | Q(body__icontains = q)).distinct()
                for post in query:
                    posts += [post]
                # community
                query = Community.objects.filter(Q(name__icontains = q) | Q(description__icontains = q)).distinct()
                for community in query:
                    searchCommunities += [community]
                # user
                query = User.objects.filter(Q(username__icontains = q)).distinct()
                for user in query:
                    users += [user]
            searchCommunities = list(set(searchCommunities))
            searchUsers = list(set(users))
            users = list(set(users))
            # convert posts to querySet
            posts = list(set(posts))
            customlist = [post.id for post in posts]
            query = Post.objects.filter(id__in = customlist)
            posts = sort(request,query)
            joinList = []
            for community in searchCommunities:
                if not request.user.is_anonymous and request.user in community.members.all():
                    joinList += [1]
                else:
                    joinList +=  [0]
            searchCommunities = zip(searchCommunities, joinList)
            followedList = []
            for u in users:
                if not request.user.is_anonymous and u in request.user.userprofile.followed_users.all():
                    followedList += [1]
                else:
                    followedList +=  [0]
            searchUsers = zip(searchUsers, followedList)
            content = {
                'posts' : posts, 
                'searchCommunities' : searchCommunities,
                'searchUsers': searchUsers,
                'users' : users,
                'search_term': request.GET.get('q'),
                'communities' : getUserCommunity(request),
                'user' : request.user,
                'joinList' : joinList,
                'action' : "Search result",
                'actionURL' : "https://i.imgur.com/Dx1kCYr.png",
            }
            return render(request, 'search/searchResult.html', content)    
    return render(request, 'search/searchResult.html')
