
from django.urls import path
from . import views 

app_name = 'userpage'

urlpatterns = [
    path('<str:user_name>/', views.userpage, name='userpage'),
    path('<str:username>/follow/', views.API_follow, name = "API_follow"),
]
