from django.shortcuts import render, redirect
from django.http import HttpResponse, Http404, JsonResponse
from django.utils import timezone
from django.views.generic import TemplateView, ListView, CreateView
from django.core.files.storage import FileSystemStorage
from django.urls import reverse_lazy
from django.contrib import messages
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.db.models import Count
from project_G3.models import Post, Comment, Community, UserProfile
from project_G3.views import index,sort,getUserCommunity
# Create your views here.

app_name = 'userpage'

def userpage(request, user_name):
    viewingUser= User.objects.get(username=user_name)
    communities = getUserCommunity(request)
    posts= viewingUser.userprofile.createdposts.all()
    posts = posts | Post.objects.filter(id__in= viewingUser.userprofile.saved_posts.all())
    posts = sort(request, posts)
    followed=0
    if request.user.is_authenticated:
        if viewingUser in request.user.userprofile.followed_users.all():
            followed=1
        data = {
            'curuser': viewingUser,
            'communities': communities,
            'posts': posts,
            'user': request.user,
            'followed': followed,
            'action' : 'u/'+user_name,
            'actionURL' : viewingUser.userprofile.avatar.url, 
        }
    else:
        data = {
            'curuser': viewingUser,
            'communities': communities,
            'followed': followed,
            'posts': posts,
            'action' : 'u/'+user_name,
            'actionURL' : viewingUser.userprofile.avatar.url, 
        }

    return render(request, 'userpage/userpage.html', data)

def API_follow(request, username):
    # Check if login.
    if not request.user.is_authenticated:
        result = {
            'loginrequired':1,
        }
        return JsonResponse(result)
    # get user
    try:
        followuser = User.objects.filter(username = username)[0]
    except:
        raise Http404("User does not exist")
    # print('--',followuser)
    result = {
        'followuser' : followuser.username,
        'state' : 'follow',
    }
    if followuser in request.user.userprofile.followed_users.all():
        request.user.userprofile.followed_users.remove(followuser)
        result['state'] = 'follow'
    else:
        request.user.userprofile.followed_users.add(followuser)
        result['state'] = 'followed'
    request.user.userprofile.save()
    return JsonResponse(result)
