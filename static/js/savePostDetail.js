function callback_save_detail(json){
    var element = document.getElementById("savebuttonDetail"+json.post_id);
    element.textContent = json.action;
    if (json.action == 'Unsave') {
        element.style = "color:red; font-weight:400;";
    } 
    else element.style = "color: rgb(173, 173, 173); font-weight:400;";


}
function savePostDetail(post_id){

    var req = new XMLHttpRequest();
    // console.log(1);
        req.onreadystatechange = function () {
            if ((req.readyState == 4) && (req.status == 200)) {
                // console.log(2);
                var json = JSON.parse(req.responseText);
                callback_save_detail(json);
            }
            else if ((req.readyState == 4) && (req.status == 500)){
                alert("You need to log in to save a post");
            }

        }

    

    // console.log(3);

    req.open('GET', '/api/post/' + post_id + '/save');
    req.send(null);
}