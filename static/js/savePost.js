function callback_save(json){
    var element = document.getElementById("save"+json.post_id);
    element.textContent = json.action;
    var savebutton = document.getElementById("savebutton"+json.post_id);

    if (json.action == 'Unsave') {
        savebutton.style = " background-color: rgba(0, 0, 0, 0.74); color: rgba(255, 255, 255, 0.950)";

    } 
    else{
        savebutton.style = "background-color: none; color: black"; 
    }
     


}
function savePost(post_id){

    var req = new XMLHttpRequest();
    // console.log(1);
        req.onreadystatechange = function () {
            if ((req.readyState == 4) && (req.status == 200)) {
                // console.log(2);
                var json = JSON.parse(req.responseText);
                callback_save(json);
            }
            else if ((req.readyState == 4) && (req.status == 500)){
                alert("You need to log in to save a post");
            }

        }

    

    // console.log(3);

    req.open('GET', '/api/post/' + post_id + '/save');
    req.send(null);
}