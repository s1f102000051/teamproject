function callback_like(json) {
    if (json.loginrequired)
        alert("You must be logged in to leave a like")

    else {
        var element = document.getElementById('like' + json.id);
        element.textContent = json.like;
        var element1 = document.getElementById('buttonlike' + json.id);
        var youElement = document.getElementById('youlikedDIV' + json.id);
        var NoneElement = document.getElementById('Nolike' + json.id);
        if (json.action == 'like') {
            element1.style = " background-color: rgba(7, 255, 28, 0.712);";
            youElement.style = "display: initial; color: white;";
            NoneElement.style = "display: none;";
        } else {
            element1.style = "";
            youElement.style = "display: none; color: white;";
            if (!json.like)
                NoneElement.style = "display: initial; ";
        }

        if (json.conflict) {
            dislike(json.id);
        }
    }

}

function callback_dislike(json) {
    if (json.loginrequired)
        alert("You must be logged in to leave a dislike")
    else {
        var element = document.getElementById('dislike' + json.id);
        element.textContent = json.dislike;
        var element1 = document.getElementById('buttondislike' + json.id);
        var youElement = document.getElementById('youdislikedDIV' + json.id);
        var NoneElement = document.getElementById('Nodislike' + json.id);
        if (json.action == 'dislike') {
            element1.style = "    background-color: rgba(255, 0, 0, 0.767);";
            youElement.style = "display: initial; color: white;";
            NoneElement.style = "display: none;";
        } else {
            element1.style = "";
            youElement.style = "display: none; color: white;";
            if (!json.dislike)
                NoneElement.style = "display: initial; ";
        }

        if (json.conflict) {
            // console.log("OMGG");
            like(json.id);
        }
    }

}

function like(article_id) {
    var req = new XMLHttpRequest();
    // console.log(1);
    req.onreadystatechange = function () {
        if ((req.readyState == 4) && (req.status == 200)) {
            // console.log(2);
            var json = JSON.parse(req.responseText);
            callback_like(json);
        }
    }
    // console.log(3);

    req.open('GET', '/api/post/' + article_id + '/like');
    req.send(null);
    // console.log(4);
    // console.log(JSON.parse(req.responseText));
    // 1 3 4 2 
}


function dislike(article_id) {
    var req = new XMLHttpRequest();
    // console.log(1);
    req.onreadystatechange = function () {
        if ((req.readyState == 4) && (req.status == 200)) {
            // console.log(2);
            var json = JSON.parse(req.responseText);
            // console.log(json);
            callback_dislike(json);
        }
    }
    // console.log(3);

    req.open('GET', '/api/post/' + article_id + '/dislike');
    req.send(null);
    // console.log(4);
    // console.log(JSON.parse(req.responseText));
    // 1 3 4 2 
}


function callme(field) {
    console.log(field.value);
    var x = document.getElementById("post_body");
    x.innerHTML=field.value;
}

function myFunction() {
    var x = document.getElementById("myDIV");
    var y = document.getElementById("post_body");
    // var content= document.getElementById("edit_post").innerHTML;
    // console.log(content);
    if (x.style.display === "none") {
        x.style.display = "block";
        y.style.display = "none";
    } else {
        x.style.display = "none";
        y.style.display = "block";
    }
}

function saveEdit() {
    alert("This post has been changed successfully!");
}

function displayPhrase()
{
    element = document.getElementById("saveButton")
    if (element.innerHTML == "save"){
        element.innerHTML ="saved";
    } 
    else {
        element.innerHTML = "save";
    }
    return True
}

function displayReplyBox(id) {
    var x = document.getElementById("reply"+id);

    if (x.style.display === "none") {
        x.style.display = "block";
    } else {
        x.style.display = "none";
    }
}