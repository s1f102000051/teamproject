from django.shortcuts import render, redirect
from .forms import registerForm, loginForm
from django.views import View
from django.contrib.auth.models import User
from django.http import HttpResponse, Http404
from django.db import IntegrityError
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from project_G3.models import UserProfile, Community
from django.utils import timezone

# Create your views here.
class registerUser(View):
    def get(self, request):
        rF = registerForm
        if 'next' in request.GET:
            next = request.GET['next']
        else:
            next = None
        return render(request, 'authentic/login.html', {'rF': rF, 'next': next})

    def post(self, request):
        if request.POST['action'] == 'SU':
            # print(request.POST['username'])
            username = request.POST['usernameSU']
            email = request.POST['emailSU']
            password = request.POST['passwordSU']
            try:
                user = User.objects.create_user(username, email, password)
            except IntegrityError as e:
                messages.info(
                    request, 'Existed username!\n You want to login? ')
                return render(request, 'authentic/login.html')
            user.save()
            player, created = UserProfile.objects.get_or_create(user=user) 
            login(request, user)
            # if 'next' in request.POST:
            #     print('--')
            #     print(request.POST['next'])
            try:
                return redirect(request.POST['nextpage'])
            except:
                # print("No thing")
                return redirect('/')
            return redirect('/')
        # print(request.POST['username'])
        else:
            username = request.POST['usernameSI']
            password = request.POST['passwordSI']
            user = authenticate(request, username=username, password=password)
            if request.user.is_authenticated:
                userProfile = UserProfile.objects.get(user = request.user)
                # print(userProfile)
                userProfile.lastLogout = timezone.now()
                userProfile.save()
                logout(request)
            if user is not None:
                login(request, user)
                try:
                    return redirect(request.POST['nextpage'])
                except:
                    # print("No thing")
                    return redirect('/')
                return redirect('/')
            else:
                messages.info(request, 'Incorrect username or password! ')
                return render(request, 'authentic/login.html')


def logoutUser(request):
    # print(-1)
    userProfile = UserProfile.objects.get(user = request.user)
    # print(userProfile)
    userProfile.lastLogout = timezone.now()
    userProfile.save()
    logout(request)
    return redirect('/')
