
from django.urls import path
from . import views

app_name = 'authentic'

urlpatterns = [
    path('login/' , views.registerUser.as_view(), name='registerUser'),
    path('logout/', views.logoutUser, name='logoutUser')

]
