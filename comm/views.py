from django.shortcuts import render
from django.shortcuts import render, redirect
from django.http import HttpResponse, Http404, JsonResponse
from django.utils import timezone
from django.views.generic import TemplateView, ListView, CreateView
from django.core.files.storage import FileSystemStorage
from django.urls import reverse_lazy
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.db.models import Q,F
from project_G3.models import Post, Comment, Community, UserProfile
from project_G3.views import index,sort,getUserCommunity,getRandomAds
import random
app_name = 'comm'
# Create your views here.
def getOnlineUsers(community):
    numOnlineUsers = 0
    for user in community.members.all():
        userProfile = UserProfile.objects.get(user = user)
        print(user)
        if (user.last_login > userProfile.lastLogout):
            numOnlineUsers+=1
    return numOnlineUsers
@login_required(login_url='/auth/login/')
def communityCreate(request):
    communities= getUserCommunity(request)
    if request.method == 'POST':
        if not request.POST["name"]:
            messages.info(request, 'Community name is required')
            return render(request, 'comm/communityCreate.html',{'communities': communities,'ads': getRandomAds(),})
        try: 
            if Community.objects.get(name = request.POST["name"]):
                messages.info(request, "This name was used for another community")
                return render(request, 'comm/communityCreate.html',{'communities': communities,'ads': getRandomAds(),})
        except:
            community = Community(
                            description = request.POST["description"],
                            name = request.POST["name"])
        # print(community)
        # background
        try:
            community.background = request.FILES['background']
        except:
            print("No background")
        # avatar
        try:
            community.avatar = request.FILES['avatar']
        except:
            print("No avatar")
        # set user as the main admin. 
        community.admin = request.user
        community.save()
        community.members.add(request.user)
        # print(community.name)
        return redirect("r/" + community.name)
    data = {
        'communities': communities, 
        'ads': getRandomAds(),
        "action" : "Create community", 
        "actionURL" : "https://i.imgur.com/jZ34VhH.jpg"
    }
    return render(request, 'comm/communityCreate.html', data)
def communityDisplay(request, community_name):
    communities= getUserCommunity(request)
    try:
        community = Community.objects.filter(name = community_name)[0]
        # print(community.name)
        # print(community.description)
        posts = Post.objects.filter(community = community)
        posts = sort(request, posts)
        content = {'community' : community,
                    'posts' : posts,
                    'communities': communities,
                    'onlineUsers' : community.getOnlineUsers(),
                    'action' : "fr/" + community.name,
                    'actionURL' : community.avatar.url,
                }
        if (request.user in community.members.all()):
            content['join'] = '1'
        # print(posts)
        return render(request, "comm/community.html", content)
    except:
        return redirect(index)

# Search inside community
def searchComm(request, community_name):
    if request.method == 'GET':
        if ('q' in request.GET):
            search_term = request.GET.get('q').split(" ")
            print(search_term, "searchComm")
            # search by posts inside community
            # Find all posts inside the community
            community = Community.objects.filter(name = community_name)[0]
            postList = Post.objects.filter(community = community)
            posts = []
            for q in search_term:
                print(q)
                # post
                query = postList.filter(Q(title__icontains = q) | Q(body__icontains = q)).distinct()
                for post in query:
                    posts += [post]
            # convert posts to querySet
            posts = list(set(posts))
            customlist = [post.id for post in posts]
            query = Post.objects.filter(id__in = customlist)
            posts = sort(request,query)
            content = {
                'posts' : posts, 
                'search_term': request.GET.get('q'),
                'community' : community,
                'communities' : getUserCommunity(request),
                'action' : "Subreddit search",
                'actionURL': 'https://i.imgur.com/Dx1kCYr.png',
            }
            return render(request, 'comm/searchComm.html', content)    
    return render(request, 'comm/searchComm.html')

def API_join(request, community_name):
    # Check if login.
    if not request.user.is_authenticated:
        result = {
            'loginrequired':1,
        }
        return JsonResponse(result)
    # get community
    try:
        community = Community.objects.filter(name = community_name)[0]
    except:
        raise Http404("Community does not exist")
    # print(community.members.all().count());
    result = {
        'community_name' : community_name,
        'members' : 0,
        'state' : 'join',
    }
    if request.user in community.members.all():
        community.members.remove(request.user)
        result['state'] = 'join'
    else:
        community.members.add(request.user)
        result['state'] = 'joined'
    result['onlineUsers'] = community.getOnlineUsers(),
    community.save()
    result['members'] = community.members.all().count()
    return JsonResponse(result)

    