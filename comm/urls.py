
from django.urls import path
from . import views 

app_name = 'comm'

urlpatterns = [
	path('<str:community_name>', views.communityDisplay, name = "communityDisplay"),
	path('<str:community_name>/search/', views.searchComm, name = "searchComm"),
	path('<str:community_name>/join/', views.API_join, name = "API_join"),
]
